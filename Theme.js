export default {
	TextInput: {
		height: 40,
		paddingHorizontal: 5,
		borderWidth: 1,
		borderColor: "#333",
	},
	Label: {
		paddingVertical: 10,
		paddingHorizontal: 5,
		backgroundColor: "yellow",
	},
	ErrorText: {
		paddingVertical: 10,
		paddingHorizontal: 5,
		color: "red",
	},
	Switch: {},
	Slider: {},
	Touchable: {
		Text: {
			color: "white",
		},
		Container: {
			backgroundColor: "purple",
			alignItems: "center",
			paddingVertical: 10,
		},
	},
	Picker: {
		Container: {
			borderColor: "#979797",
			borderWidth: 1,
			borderRadius: 5,
			padding: 5,
			height: 37,
			backgroundColor: "#f7f7f7",
		},
		// Android only
		Android: {
			height: 27,
		},
		// iOS only
		IOS: {
			Text: {
				backgroundColor: "transparent",
			},
			//Modal
			Container: {
				backgroundColor: "transparent",
				margin: 10,
			},
			Accept: {
				color: "#004180",
				fontWeight: "bold",
			},
		},
	},
};
