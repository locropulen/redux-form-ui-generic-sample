import { reduxForm } from "redux-form"; // 7.2.0
import { rules, runner } from "redux-form-ui"; // 0.1.2

import MyReduxForm from "./MyReduxForm";

import "react-redux"; // 5.0.6
import "redux"; // 3.7.2

export default reduxForm({
	form: "example",
	initialValues: {
		name: "Jane Doe",
		slider: 0,
	},
	validate: values =>
		runner.run(values, [runner.ruleRunner("name", "Name", rules.required)]),
})(MyReduxForm);
