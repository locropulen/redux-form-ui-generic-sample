import React, { PureComponent } from "react";

import { Field } from "redux-form"; // 7.2.0

import { ScrollView } from "react-native";
import NativeInput from "../components/NativeInput";
import { GenericField, BoolField, normalize } from "redux-form-ui"; // 0.1.4

import NativeTouchable from "../components/NativeTouchable";
import NativeSwitch from "../components/NativeSwitch";
import NativeSlider from "../components/NativeSlider";

const NameTextField = GenericField(NativeInput);
const PhoneTextField = GenericField(NativeInput);
const SliderField = GenericField(NativeSlider);
const SwitchField = BoolField(NativeSwitch);

export default class MyForm extends PureComponent {
	componentDidMount() {
		this.ref // the Field
			.getRenderedComponent()
			.getRenderedComponent()
			.focus();
	}
	saveRef = ref => (this.ref = ref);
	onPress = () => {
		this.props.handleSubmit();
	};
	render() {
		return [
			<NativeTouchable key={0} onPress={this.onPress}>
				Click Me!
			</NativeTouchable>,
			<ScrollView key={1}>
				<Field
					name="name"
					component={NameTextField}
					hintText="Name"
					ref={this.saveRef}
					withRef
					floatingLabelText="Name"
					placeholder="Name"
				/>
				<Field
					name="phone"
					component={PhoneTextField}
					normalize={normalize.toPhone}
					keyboardType="phone-pad"
					hintText="Phone"
					floatingLabelText="Phone"
					placeholder="Phone"
				/>
				<Field
					name="switch"
					component={SwitchField}
					floatingLabelText="Switch me"
				/>
				<Field
					name="slider"
					component={SliderField}
					floatingLabelText="Slide me"
					minimumValue={0}
					maximumValue={10}
				/>
			</ScrollView>,
		];
	}
}
